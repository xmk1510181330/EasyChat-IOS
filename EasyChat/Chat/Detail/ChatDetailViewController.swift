//
//  ChatDetailViewController.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/24.
//

import UIKit

/** 聊天主页面*/
class ChatDetailViewController: UIViewController {
    /** 顶部视图*/
    let topView : UIView = UIView.init()
    let backView : UIImageView = UIImageView.init()  //返回按钮
    let nickName : UILabel = UILabel.init()  //昵称
    let stateLabel : UILabel = UILabel.init()  //状态
    let settingView : UIImageView = UIImageView.init()  //聊天设置
    let phoneView : UIImageView = UIImageView.init()  //视频通话
    /** 聊天消息内容区域*/
    let chatView : UITableView = UITableView.init()
    /** 底部工具栏*/
    let buttomToolView : UIView = UIView.init()
    var optionsView : ChatBottomOptionView?  //选项栏
    let searchView : UIView = UIView.init()  //输入区域
    let searchField : UserDefineTextField = UserDefineTextField.init() //输入框
    //声音
    var voiceView : ChatVoiceView?
    //红包
    var envelopeView : ChatEnvelopeView?
    //表情
    var emojView : ChatEmojiView?
    //更多
    var moreView : ChatMoreView?
    /** 当前聊天好友的信息*/
    var mateModel : MateModel?
    var groupModel : GroupModel?
    //var messageList : [EasyChatC2CResponse] = []  //聊天消息
    var messageHeight : [CGFloat] = []  //聊天消息的高度
    /** 自己的一点信息*/
    var meModel : MateModel = AppDelegate.userInfos[AppDelegate.meInfo.number]!
    /** OSS上传工具*/
    let ossUtils : QiniuOSSUtils = QiniuOSSUtils.init()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.view.backgroundColor = UIColor.init(red: 0.92, green: 0.93, blue: 0.99, alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    convenience init(mateInfo : MateModel, bindVC : ChatViewController) {
        self.init()
        mateModel = mateInfo
        /** 顶部视图*/
        topView.frame = CGRect.init(x: 0, y: stateRegionHeight, width: screenWidth, height: 92-stateRegionHeight)
        topView.backgroundColor = UIColor.init(red: 0.92, green: 0.93, blue: 0.99, alpha: 1)
        self.view.addSubview(topView)
        nickName.text = mateInfo.mateName
        nickName.textColor = UIColor.black
        nickName.font = UIFont.systemFont(ofSize: 15)
        nickName.sizeToFit()
        nickName.frame.origin = CGPoint.init(x: (topView.frame.width-nickName.frame.width)/2, y: 7)
        topView.addSubview(nickName)
        backView.frame = CGRect.init(x: 12, y: (topView.frame.height-22)/2, width: 22, height: 22)
        backView.image = UIImage.init(named: "chat_back")
        backView.isUserInteractionEnabled = true
        let backRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(backHandler))
        backView.addGestureRecognizer(backRecognizer)
        topView.addSubview(backView)
        settingView.frame = CGRect.init(x: topView.frame.width-15-20, y: topView.frame.height-15-20, width: 20, height: 20)
        settingView.image = UIImage.init(named: "chat_setting")
        settingView.isUserInteractionEnabled = true
        let settingRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(settingHandler))
        settingView.addGestureRecognizer(settingRecognizer)
        topView.addSubview(settingView)
        phoneView.frame = CGRect.init(x: settingView.frame.minX-15-22, y: topView.frame.height-15-20, width: 22, height: 22)
        phoneView.image = UIImage.init(named: "chat_phone")
        phoneView.isUserInteractionEnabled = true
        let phoneRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(phoneHandler))
        phoneView.addGestureRecognizer(phoneRecognizer)
        topView.addSubview(phoneView)
        /** 底部工具栏，包含输入框和选项栏*/
        //buttomToolView.frame = CGRect.init(x: 0, y: screenHeight-122-50, width: screenWidth, height: 90)
        buttomToolView.frame = CGRect.init(x: 0, y: screenHeight-bottomRegionHeight-40-50, width: screenWidth, height: 50+40)
        buttomToolView.backgroundColor = UIColor.init(red: 0.92, green: 0.93, blue: 0.99, alpha: 1)
        self.view.addSubview(buttomToolView)
        //输入区域
        searchView.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 50)
        searchView.backgroundColor = UIColor.init(red: 0.92, green: 0.93, blue: 0.99, alpha: 1)
        buttomToolView.addSubview(searchView)
        //输入框
        searchField.frame = CGRect.init(x: 8, y: 10, width: screenWidth-16, height: 36)
        searchField.backgroundColor = UIColor.white
        searchField.layer.masksToBounds = true
        searchField.layer.cornerRadius = 18
        searchField.keyboardType = .default
        searchField.returnKeyType = .send
        searchField.delegate = self
        searchView.addSubview(searchField)  //输入框放到合适的位置，方便弹起
        //监听键盘的高度变化
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChangeFrame(node:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        //选项栏
        optionsView = ChatBottomOptionView.init(frame: CGRect.init(x: 0, y: searchView.frame.maxY, width: screenWidth, height: 40), bind: self)
//        optionsView?.setImageCompleted(colsure: { image, url in
//            self.imageMessageHandler(image: image, url: url)
//        })
        //设置闭包回调
        setOptionsColsure()
        buttomToolView.addSubview(optionsView!)
        /** 聊天内容区域*/
        chatView.frame = CGRect.init(x: 0, y: topView.frame.maxY, width: screenWidth, height: buttomToolView.frame.minY-topView.frame.maxY)
        chatView.backgroundColor = UIColor.init(red: 0.90, green: 0.91, blue: 0.96, alpha: 1)
        chatView.delegate = self
        chatView.dataSource = self
        chatView.tableFooterView =  UIView.init(frame: CGRect.zero) //隐藏多余的格子
        chatView.separatorStyle = .none  //隐藏格子之间的分割线
        chatView.allowsSelection = false  //禁用掉表格的选中效果
        chatView.register(ChatDetailViewCell.self, forCellReuseIdentifier: "chatMessageCell")  //注册cell
        self.view.addSubview(chatView)
        //声音
        voiceView = ChatVoiceView.init(frame: CGRect.init(x: 0, y: self.view.frame.height-bottomRegionHeight-250, width: screenWidth, height: 250))
        voiceView?.isHidden = true
        self.view.addSubview(voiceView!)
        //红包
        envelopeView = ChatEnvelopeView.init(frame: CGRect.init(x: 0, y: self.view.frame.height-bottomRegionHeight-250, width: screenWidth, height: 250))
        envelopeView?.isHidden = true
        self.view.addSubview(envelopeView!)
        //表情
        emojView = ChatEmojiView.init(frame: CGRect.init(x: 0, y: self.view.frame.height-bottomRegionHeight-250, width: screenWidth, height: 250))
        emojView?.isHidden = true
        self.view.addSubview(emojView!)
        //更多
        moreView = ChatMoreView.init(frame: CGRect.init(x: 0, y: self.view.frame.height-bottomRegionHeight-250, width: screenWidth, height: 250))
        moreView?.isHidden = true
        self.view.addSubview(moreView!)
        //把新的消息添加进来
        for _ in ChatMessageStock.instance.mateMessageList[mateInfo.mateNumber]! {
            messageHeight.append(60)
        }
        //让他也能感知到有新消息进来
        bindVC.setCatchNewMessage {
            DispatchQueue.main.async {
                self.chatView.reloadData()
            }
        }
    }
    
    convenience init(groupInfo : GroupModel, bindVC : ChatViewController) {
        self.init()
        groupModel = groupInfo
        /** 顶部视图*/
        topView.frame = CGRect.init(x: 0, y: stateRegionHeight, width: screenWidth, height: 92-stateRegionHeight)
        topView.backgroundColor = UIColor.init(red: 0.92, green: 0.93, blue: 0.99, alpha: 1)
        self.view.addSubview(topView)
        nickName.text = groupModel?.groupName
        nickName.textColor = UIColor.black
        nickName.font = UIFont.systemFont(ofSize: 15)
        nickName.sizeToFit()
        nickName.frame.origin = CGPoint.init(x: (topView.frame.width-nickName.frame.width)/2, y: 7)
        topView.addSubview(nickName)
        backView.frame = CGRect.init(x: 12, y: (topView.frame.height-22)/2, width: 22, height: 22)
        backView.image = UIImage.init(named: "chat_back")
        backView.isUserInteractionEnabled = true
        let backRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(backHandler))
        backView.addGestureRecognizer(backRecognizer)
        topView.addSubview(backView)
        settingView.frame = CGRect.init(x: topView.frame.width-15-20, y: topView.frame.height-15-20, width: 20, height: 20)
        settingView.image = UIImage.init(named: "chat_setting")
        settingView.isUserInteractionEnabled = true
        let settingRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(settingHandler))
        settingView.addGestureRecognizer(settingRecognizer)
        topView.addSubview(settingView)
        phoneView.frame = CGRect.init(x: settingView.frame.minX-15-22, y: topView.frame.height-15-20, width: 22, height: 22)
        phoneView.image = UIImage.init(named: "chat_phone")
        phoneView.isUserInteractionEnabled = true
        let phoneRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(phoneHandler))
        phoneView.addGestureRecognizer(phoneRecognizer)
        topView.addSubview(phoneView)
        /** 底部工具栏，包含输入框和选项栏*/
        //buttomToolView.frame = CGRect.init(x: 0, y: screenHeight-122-50, width: screenWidth, height: 90)
        buttomToolView.frame = CGRect.init(x: 0, y: screenHeight-bottomRegionHeight-40-50, width: screenWidth, height: 50+40)
        buttomToolView.backgroundColor = UIColor.init(red: 0.92, green: 0.93, blue: 0.99, alpha: 1)
        self.view.addSubview(buttomToolView)
        //输入区域
        searchView.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 50)
        searchView.backgroundColor = UIColor.init(red: 0.92, green: 0.93, blue: 0.99, alpha: 1)
        buttomToolView.addSubview(searchView)
        //输入框
        searchField.frame = CGRect.init(x: 8, y: 10, width: screenWidth-16, height: 36)
        searchField.backgroundColor = UIColor.white
        searchField.layer.masksToBounds = true
        searchField.layer.cornerRadius = 18
        searchField.keyboardType = .default
        searchField.returnKeyType = .send
        searchField.delegate = self
        searchView.addSubview(searchField)  //输入框放到合适的位置，方便弹起
        //监听键盘的高度变化
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChangeFrame(node:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        //选项栏
        optionsView = ChatBottomOptionView.init(frame: CGRect.init(x: 0, y: buttomToolView.frame.height-40, width: screenWidth, height: 40), bind: self)
        optionsView?.setImageCompleted(colsure: { image, url in
            self.imageMessageHandler(image: image, url: url)
        })
        buttomToolView.addSubview(optionsView!)
        /** 聊天内容区域*/
        chatView.frame = CGRect.init(x: 0, y: topView.frame.maxY, width: screenWidth, height: buttomToolView.frame.minY-topView.frame.maxY)
        chatView.backgroundColor = UIColor.init(red: 0.90, green: 0.91, blue: 0.96, alpha: 1)
        chatView.delegate = self
        chatView.dataSource = self
        chatView.tableFooterView =  UIView.init(frame: CGRect.zero) //隐藏多余的格子
        chatView.separatorStyle = .none  //隐藏格子之间的分割线
        chatView.allowsSelection = false  //禁用掉表格的选中效果
        chatView.register(ChatDetailViewCell.self, forCellReuseIdentifier: "chatMessageCell")  //注册cell
        self.view.addSubview(chatView)
//        //输入区域
//        searchView.frame = CGRect.init(x: buttomToolView.frame.minX, y: buttomToolView.frame.minY, width: screenWidth, height: 50)
//        searchView.backgroundColor = UIColor.init(red: 0.92, green: 0.93, blue: 0.99, alpha: 1)
//        self.view.addSubview(searchView)
//        //输入框
//        searchField.frame = CGRect.init(x: 8, y: 10, width: screenWidth-16, height: 36)
//        searchField.backgroundColor = UIColor.white
//        searchField.layer.masksToBounds = true
//        searchField.layer.cornerRadius = 18
//        searchField.keyboardType = .default
//        searchField.returnKeyType = .send
//        searchField.delegate = self
//        searchView.addSubview(searchField)  //输入框放到合适的位置，方便弹起
//        //监听键盘的高度变化
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChangeFrame(node:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        //把新的消息添加进来
        for _ in ChatMessageStock.instance.mateMessageList[groupModel!.groupNumber]! {
            messageHeight.append(60)
        }
        //让他也能感知到有新消息进来
        bindVC.setCatchNewMessage {
            DispatchQueue.main.async {
                self.chatView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setOptionsColsure() {
        optionsView?.setSubView(colsure: { [self] tag, isClose in
            voiceView?.isHidden = true
            envelopeView?.isHidden = true
            emojView?.isHidden = true
            moreView?.isHidden = true
            if isClose {  //关闭
                buttomToolView.frame.origin = CGPoint.init(x: buttomToolView.frame.origin.x, y: screenHeight-bottomRegionHeight-40-50)
                chatView.frame.size = CGSize.init(width: chatView.frame.size.width, height: buttomToolView.frame.minY-topView.frame.maxY)
            }else { //展开
                switch tag {
                case 0:
                    voiceView?.isHidden = false
                case 1:
                    PhotoTools.instance.setTargetImage { [self] targetImage in
                        let url = ossUtils.simpleUpload(target: targetImage)
                        imageMessageHandler(image: targetImage.chatImage(), url: url)
                    }
                    PhotoTools.instance.goPhoto(bindVC: self)
                case 2:
                    print("相机")
                case 3:
                    envelopeView?.isHidden = false
                case 4:
                    emojView?.isHidden = false
                case 5:
                    moreView?.isHidden = false
                default:
                    print("未知选项")
                }
                buttomToolView.frame.origin = CGPoint.init(x: buttomToolView.frame.origin.x, y: emojView!.frame.minY-buttomToolView.frame.height)
                chatView.frame.size = CGSize.init(width: chatView.frame.size.width, height: buttomToolView.frame.minY-topView.frame.maxY)
            }
        })
    }
    
    /** 发送图片消息*/
    func imageMessageHandler(image : UIImage, url : String) {
        if groupModel != nil {  //群聊
            let selfMessage = EasyChatGroupResponse.init()
            //selfMessage.content = msg
            
            selfMessage.dateTime = "2021-12-25"
            selfMessage.groupID = groupModel!.groupNumber
            selfMessage.fromNumber = meModel.mateNumber
            ChatMessageStock.instance.addGroupResponse(groupResponse: selfMessage)
            //向服务器发送消息
            EasyChatSocketTool.instance.sendGroupMessage(content: url, groupId: groupModel!.groupNumber)
        }else {  //私聊
            let selfRequest = EasyChatC2CRequest.init()
            selfRequest.fromNumber = meModel.mateNumber
            selfRequest.toNumber = mateModel!.mateNumber
            //selfRequest.content = msg
            selfRequest.image = image
            ChatMessageStock.instance.addC2CRequest(c2cRequest: selfRequest)
            //向服务器发送消息
            EasyChatSocketTool.instance.sendC2CMessage(content: url, toNumber: mateModel!.mateNumber)
        }
        //刷新列表
        DispatchQueue.main.async { [self] in
            chatView.reloadData()
        }
    }

    /** 返回上一级*/
    @objc func backHandler() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func settingHandler() {
        print("设置")
    }
    
    @objc func phoneHandler() {
        print("视频通话")
    }
    
    //监听键盘的升起时间，让输入框伴随键盘一起起来
    @objc func keyboardWillChangeFrame(node : Notification) {
        // 1.获取动画执行的时间
        let duration = node.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        
        // 2.获取键盘最终 Y值
        let endFrame = (node.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let y = endFrame.origin.y
        let fixY : CGFloat = y - buttomToolView.frame.height
        
        // 3.变换搜索区域的位置
        if y == screenHeight {
            //收起键盘
            UIView.animate(withDuration: duration) { [self] in
                buttomToolView.frame.origin = CGPoint.init(x: buttomToolView.frame.origin.x, y: screenHeight-bottomRegionHeight-40-50)
                chatView.frame.size = CGSize.init(width: chatView.frame.size.width, height: buttomToolView.frame.minY-topView.frame.maxY)
            }

        }else {
            //弹出键盘
            UIView.animate(withDuration: duration) { [self] in
                buttomToolView.frame.origin = CGPoint.init(x: buttomToolView.frame.origin.x, y: fixY)
                chatView.frame.size = CGSize.init(width: chatView.frame.size.width, height: buttomToolView.frame.minY-topView.frame.maxY)
            }
        }
//        let targetIndex = IndexPath.init(row: messageHeight.count-1, section: 0)
//        chatView.scrollToRow(at: targetIndex, at: .top, animated: true)
//        let bias = chatView.contentSize.height-chatView.bounds.height
//        if bias > 0 {
//            chatView.setContentOffset(CGPoint.init(x: chatView.contentOffset.x, y: bias), animated: true)
//        }
    }
}
/** 键盘相关的回调*/
extension ChatDetailViewController: UITextFieldDelegate{
    
    //用户点击发送以后的回调
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchField.resignFirstResponder()
        //记录输入框的输入内容
        let msg : String = searchField.text!
        searchField.text = ""  //清空输入框
        if msg == "" {
            return true
        }
        if groupModel != nil {  //群聊
            let selfMessage = EasyChatGroupResponse.init()
            selfMessage.content = msg
            selfMessage.dateTime = "2021-12-25"
            selfMessage.groupID = groupModel!.groupNumber
            selfMessage.fromNumber = meModel.mateNumber
            ChatMessageStock.instance.addGroupResponse(groupResponse: selfMessage)
            //向服务器发送消息
            EasyChatSocketTool.instance.sendGroupMessage(content: msg, groupId: groupModel!.groupNumber)
        }else {  //私聊
            let selfRequest = EasyChatC2CRequest.init()
            selfRequest.fromNumber = meModel.mateNumber
            selfRequest.toNumber = mateModel!.mateNumber
            selfRequest.content = msg
            ChatMessageStock.instance.addC2CRequest(c2cRequest: selfRequest)
            //向服务器发送消息
            EasyChatSocketTool.instance.sendC2CMessage(content: msg, toNumber: mateModel!.mateNumber)
        }
        //messageHeight.append(60.0)
        //刷新列表
        DispatchQueue.main.async { [self] in
            chatView.reloadData()
            chatView.layoutIfNeeded()  //强制刷新并且阻塞执行
            let targetIndex = IndexPath.init(row: messageHeight.count-1, section: 0)
            chatView.scrollToRow(at: targetIndex, at: .top, animated: true)
//            let bias = chatView.contentSize.height-chatView.bounds.height
//            if bias > 0 {
//                chatView.setContentOffset(CGPoint.init(x: chatView.contentOffset.x, y: bias), animated: true)
//            }
        }
        return true
    }
}
/** 消息列表相关的回调*/
extension ChatDetailViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if groupModel != nil {
            //print("预计的个数: ", ChatMessageStock.instance.mateMessageList[groupModel!.groupNumber]!.count)
            return ChatMessageStock.instance.mateMessageList[groupModel!.groupNumber]!.count
        }else {
            //print(ChatMessageStock.instance.mateMessageList[mateModel!.mateNumber]!.count)
            return ChatMessageStock.instance.mateMessageList[mateModel!.mateNumber]!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatMessageCell") as! ChatDetailViewCell
        var rowHeight : CGFloat = 60.0
        if groupModel != nil {
            let message = ChatMessageStock.instance.mateMessageList[groupModel!.groupNumber]![indexPath.row]
            ///不同的消息cell用不同的标识符
            if message.fromID == AppDelegate.meInfo.number {
                //是自己的发言
                rowHeight = cell.setInfo(image: UIImage.init(named: meModel.matePhote)!, message: message.content, isBelongSelf: true)
            }else {
                //是其他群友的消息
                rowHeight = cell.setInfo(image: UIImage.init(named: AppDelegate.userInfos[message.fromID]!.matePhote)!, message: message.content, isBelongSelf: false)
            }
        }else {
            let message = ChatMessageStock.instance.mateMessageList[mateModel!.mateNumber]![indexPath.row]
            if message.fromID == AppDelegate.meInfo.number {
                //是自己的发言
                if message.image != nil {
                    rowHeight = cell.setImageInfo(image: UIImage.init(named: meModel.matePhote)!, message: message.image!, isBelongSelf: true)
                }else {
                    rowHeight = cell.setInfo(image: UIImage.init(named: meModel.matePhote)!, message: message.content, isBelongSelf: true)
                }
            }else {
                //是其他群友的消息
                if message.image != nil {
                    rowHeight = cell.setImageInfo(image: UIImage.init(named: mateModel!.matePhote)!, message: message.image!, isBelongSelf: false)
                }else {
                    rowHeight = cell.setInfo(image: UIImage.init(named: mateModel!.matePhote)!, message: message.content, isBelongSelf: false)
                }
            }
        }
        
        /**
         * 这些delegate的执行顺序貌似有些问题
         */
        //print(messageHeight.count, "--", indexPath.row)
        if messageHeight.count <= indexPath.row {
            messageHeight.append(rowHeight)
            //print("实际上的个数: ", messageHeight.count)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //print("出错位置的个数: ", indexPath.row)
        return messageHeight[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
}
