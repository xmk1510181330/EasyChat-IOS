//
//  ChatDetailViewCell.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/25.
//

import UIKit

/** 聊天详情页的列表Cell*/
class ChatDetailViewCell: UITableViewCell {
    /** 用户头像*/
    let photoView : UIImageView = UIImageView.init()
    /** 聊天气泡*/
    var messageView : ChatMessageView?
    /** 聊天图片*/
    var messageImage : UIImageView?
    /** 聊天图片的原图*/
    var messageOriginImage : UIImage?
    /** 小黑*/
    var blackView : UIView?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    /** 复用之前调用的函数*/
    override func prepareForReuse() {
        super.prepareForReuse()
        //复用之前去掉原先cell的视图，避免内容混乱
        for subview in self.contentView.subviews {
            subview.removeFromSuperview()
        }
    }
    
    /** 图片消息的赋值方法，返回图片消息的高度*/
    func setImageInfo(image : UIImage, message : UIImage, isBelongSelf : Bool) -> CGFloat{
        self.frame.size = CGSize.init(width: screenWidth, height: 60)
        self.backgroundColor = UIColor.init(red: 0.90, green: 0.91, blue: 0.96, alpha: 1)
        //self.backgroundColor = UIColor.yellow
        if isBelongSelf {
            //自己的发言，在右边
            photoView.frame = CGRect.init(x: self.frame.width-15-40, y: 10, width: 40, height: 40)
            //聊天图片
            messageImage = UIImageView.init(frame: CGRect.init(x: photoView.frame.minX-5-message.size.width, y: photoView.frame.minY+2, width: message.size.width, height: message.size.height))
        }else {
            //好友的消息，在左边
            photoView.frame = CGRect.init(x: 15, y: 10, width: 40, height: 40)
            //聊天图片
            messageImage = UIImageView.init(frame: CGRect.init(x: photoView.frame.maxX+5, y: photoView.frame.minY+2, width: message.size.width, height: message.size.height))
        }
        photoView.image = image
        photoView.layer.masksToBounds = true
        photoView.layer.cornerRadius = 20
        self.contentView.addSubview(photoView)
        messageImage?.image = message
        //设置图片圆角
        messageImage?.layer.masksToBounds = true
        messageImage?.layer.cornerRadius = 15
        //添加图片的点击事件
        messageImage?.isUserInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(tapImageMessage))
        messageImage?.addGestureRecognizer(tapRecognizer)
        self.contentView.addSubview(messageImage!)
        return message.size.height+10
    }
    
    @objc func tapImageMessage() {
        let baseView = self.superview?.superview
        blackView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: baseView!.frame.width, height: baseView!.frame.height))
        blackView!.backgroundColor = UIColor.black
        self.superview?.superview?.addSubview(blackView!)
        // eh/ew == xx/screenWidth
        //把照片贴上去
        let origin : UIImage = messageImage!.image!
        let fixRatio : CGFloat = origin.size.height / origin.size.width
        let height : CGFloat = origin.size.height * fixRatio
        let useImageView = UIImageView.init(frame: CGRect.init(x: 0, y: (blackView!.frame.height-height)/2, width: screenWidth, height: height))
        useImageView.image = origin.scaleImage(scaleSize: screenWidth/origin.size.width)
        blackView?.addSubview(useImageView)
        blackView!.isUserInteractionEnabled = true
        let tapBlackViewRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(tapBlackViewHandler))
        blackView!.addGestureRecognizer(tapBlackViewRecognizer)
    }
    
    @objc func tapBlackViewHandler() {
        blackView!.removeFromSuperview()
    }

    /** 普通文本消息的赋值方法，返回文本信息的高度*/
    func setInfo(image : UIImage, message : String, isBelongSelf : Bool) -> CGFloat {
        self.frame.size = CGSize.init(width: screenWidth, height: 60)
        self.backgroundColor = UIColor.init(red: 0.90, green: 0.91, blue: 0.96, alpha: 1)
        //self.backgroundColor = UIColor.yellow
        //头像
        if isBelongSelf {
            //自己的发言，在右边
            photoView.frame = CGRect.init(x: self.frame.width-15-40, y: 10, width: 40, height: 40)
            //聊天气泡
            messageView = ChatMessageView.init(frame: CGRect.init(x: photoView.frame.minX-5, y: photoView.frame.minY+2, width: 10, height: 10), text: message, belongSelf: isBelongSelf)
            messageView?.frame.origin = CGPoint.init(x: photoView.frame.minX-5-messageView!.frame.width, y: photoView.frame.minY+2)
            
        }else {
            //好友的消息，在左边
            photoView.frame = CGRect.init(x: 15, y: 10, width: 40, height: 40)
            //聊天气泡
            messageView = ChatMessageView.init(frame: CGRect.init(x: photoView.frame.maxX+5, y: photoView.frame.minY+2, width: 10, height: 10), text: message, belongSelf: isBelongSelf)
        }
        photoView.image = image
        photoView.layer.masksToBounds = true
        photoView.layer.cornerRadius = 20
        self.contentView.addSubview(photoView)
        messageView?.backgroundColor = UIColor.init(red: 0.90, green: 0.91, blue: 0.96, alpha: 1)
        self.contentView.addSubview(messageView!)
        //根据气泡再去变换Cell的尺寸
        self.frame.size = CGSize.init(width: self.frame.size.height, height: messageView!.frame.height+10)
        return messageView?.frame.maxY ?? 60
    }
}
