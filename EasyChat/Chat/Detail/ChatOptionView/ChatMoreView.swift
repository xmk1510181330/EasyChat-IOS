//
//  ChatMoreView.swift
//  EasyChat
//
//  Created by vase on 2022/1/25.
//

import UIKit

/** 更多*/
class ChatMoreView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.yellow
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
