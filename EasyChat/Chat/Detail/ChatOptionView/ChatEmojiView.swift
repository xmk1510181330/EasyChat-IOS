//
//  ChatEmojiView.swift
//  EasyChat
//
//  Created by vase on 2022/1/25.
//

import UIKit

/** 表情*/
class ChatEmojiView: UIView {
    /** 表情标签*/
    var emojLabel : UILabel = UILabel.init()
    /** 表情集合*/
    var emojCollectionView : UICollectionView?
    
    /**
     * 记录一下现在的问题，我的表情列表做出来了，但是IOS系统显然是不认我的表情编码的
     * 那么，问题就是：怎么让输入框把对应的表情编码显示成对应的表情
     */
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        //表情标签
        emojLabel.text = "经典emoj"
        emojLabel.textColor = UIColor.init(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        emojLabel.font = UIFont.systemFont(ofSize: 10)
        emojLabel.sizeToFit()
        emojLabel.frame.origin = CGPoint.init(x: 15, y: 15)
        self.addSubview(emojLabel)
        //样式
        let flowLayout = UICollectionViewFlowLayout.init()
        flowLayout.minimumLineSpacing = 20  //行最小间距
        flowLayout.minimumInteritemSpacing = 20 //行内Item最小间距
        //cell的大小
        flowLayout.itemSize = CGSize.init(width: 30, height: 30)
        emojCollectionView = UICollectionView.init(frame: CGRect.init(x: 15, y: emojLabel.frame.maxY+15, width: frame.width-30, height: frame.height-emojLabel.frame.maxY-15), collectionViewLayout: flowLayout)
        emojCollectionView?.register(ChatEmojViewCell.self, forCellWithReuseIdentifier: "chatEmojCell")
        emojCollectionView?.delegate = self
        emojCollectionView?.dataSource = self
        emojCollectionView?.backgroundColor = UIColor.white
        self.addSubview(emojCollectionView!)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

extension ChatEmojiView : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 212
    }

        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "chatEmojCell", for: indexPath) as! ChatEmojViewCell
        cell.setIndex(i: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
}
