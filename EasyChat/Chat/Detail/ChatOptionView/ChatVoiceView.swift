//
//  ChatVoiceView.swift
//  EasyChat
//
//  Created by vase on 2022/1/25.
//

import UIKit

/** 声音*/
class ChatVoiceView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.blue
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
