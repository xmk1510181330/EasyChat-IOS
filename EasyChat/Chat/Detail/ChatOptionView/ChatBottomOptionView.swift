//
//  ChatBottomOptionView.swift
//  EasyChat
//
//  Created by vase on 2022/1/16.
//

import UIKit

/** 聊天页面的底部工具栏*/
class ChatBottomOptionView: UIView {
    /** 绑定的VC*/
    var bindVC : ChatDetailViewController?
    /** 按钮组名称*/
    let optionNames : [String] = ["chat_vioce", "chat_image", "detail_camera", "chat_hongbao", "chat_emoji", "detail_more"]
    let optionSelectedNames : [String] = ["chat_voice_selected", "chat_image_selected", "detail_camera_selected", "chat_hongbao_selected", "chat_emoji_selected", "detail_more_selected"]
    //当前所有的按钮集合
    var optionButtons : [UIButton] = []
    //当前选中的按钮下标
    var activeIndex : Int = -1
    /** 图片消息返回的闭包*/
    var imageCompletedColsure : ((UIImage, String)->Void)?
    /** 创建子视图的闭包*/
    var subViewColsure : ((Int, Bool)->Void)?
    /** OSS上传工具*/
    let ossUtils : QiniuOSSUtils = QiniuOSSUtils.init()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.init(red: 0.92, green: 0.93, blue: 0.99, alpha: 1)
        let buttonWidth : CGFloat = 28.0  //按钮宽度
        var fixX : CGFloat = 30 //浮动起始位置
        let interVal : CGFloat = (screenWidth-60-6*buttonWidth)/5
        for i in 0..<optionNames.count {
            let button = UIButton.init(frame: CGRect.init(x: fixX, y: (frame.height-buttonWidth)/2, width: buttonWidth, height: buttonWidth))
            button.tag = i
            button.setBackgroundImage(UIImage.init(named: optionNames[i]), for: .normal)
            button.setBackgroundImage(UIImage.init(named: optionSelectedNames[i]), for: .selected)
            button.addTarget(self, action: #selector(optionHandler(button:)), for: .touchUpInside)
            optionButtons.append(button)
            self.addSubview(button)
            fixX = fixX + buttonWidth + interVal
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    convenience init(frame: CGRect, bind : ChatDetailViewController) {
        self.init(frame: frame)
        bindVC = bind
    }
    
    func setImageCompleted(colsure : @escaping ((UIImage, String)->Void)) {
        imageCompletedColsure = colsure
    }
    
    func setSubView(colsure : @escaping ((Int, Bool)->Void)) {
        subViewColsure = colsure
    }
    
    @objc func optionHandler(button : UIButton) {
        //去掉之前按钮的选中效果
        var isClose : Bool = false
        if activeIndex != -1 {
            optionButtons[activeIndex].isSelected = false
        }
        //重复点击表示关闭
        if activeIndex == button.tag {
            isClose = true
        }
        //给新选中的按钮加选中效果
        activeIndex = button.tag
        optionButtons[activeIndex].isSelected = true
        //执行各自的操作
        if subViewColsure != nil {
            subViewColsure!(button.tag, isClose)
        }
//        switch button.tag {
//        case 0:
//            print("语音")
//        case 1:
//            PhotoTools.instance.setTargetImage { [self] image in
//                if imageCompletedColsure != nil {
//                    let url = ossUtils.simpleUpload(target: image)
//                    imageCompletedColsure!(image.chatImage(), url)
//                }
//            }
//            PhotoTools.instance.goPhoto(bindVC: self.bindVC!)
//        case 2:
//            print("相机")
//        case 3:
//            print("红包")
//        case 4:
//
//        case 5:
//            print("更多")
//        default:
//            print("默认")
//        }
    }
    
    
}
