//
//  ChatTableViewCell.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/7.
//

import UIKit

class MessageTableViewCell: UITableViewCell {
    /** 好友头像*/
    let profileView : UIImageView = UIImageView.init()
    /** 好友昵称*/
    let mateLabel : UILabel = UILabel.init()
    /** 消息*/
    let messageLabel : UILabel = UILabel.init()
    /** 消息时间戳*/
    let messageDate : UILabel = UILabel.init()
    /** 未读消息--小圆点*/
    var count : MessageCountView?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        /// profileView
        profileView.frame = CGRect.init(x: 16, y: 75-12-48, width: 48, height: 48)
        profileView.image = UIImage.init(named: "chat_mateProfile1")
        profileView.layer.masksToBounds = true
        profileView.layer.cornerRadius = 24
        self.addSubview(profileView)
        /// mateLabel
        mateLabel.frame = CGRect.init(x: profileView.frame.maxX+12, y: profileView.frame.minY+5, width: 10, height: 15)
        mateLabel.text = "林深时见鹿der"
        mateLabel.textAlignment = .left
        mateLabel.sizeToFit()
        mateLabel.textColor = UIColor.black
        mateLabel.font = UIFont.systemFont(ofSize: 15)
        self.addSubview(mateLabel)
        /// messageLabel
        messageLabel.frame = CGRect.init(x: profileView.frame.maxX+12, y: mateLabel.frame.maxY+5, width: 10, height: 13)
        messageLabel.text = "晚上来家里吃饭，别忘了"
        messageLabel.textAlignment = .left
        messageLabel.sizeToFit()
        messageLabel.font = UIFont.systemFont(ofSize: 13)
        messageLabel.textColor = UIColor.init(red: 0.45, green: 0.46, blue: 0.53, alpha: 1)
        self.addSubview(messageLabel)
        /// messageDate
        messageDate.frame = CGRect.init(x: screenWidth-16-68, y: profileView.frame.minY+5, width: 68, height: 12)
        messageDate.text = "2021-11-30"
        messageDate.textAlignment = .right
        messageDate.font = UIFont.systemFont(ofSize: 12)
        messageDate.textColor = UIColor.init(red: 0.45, green: 0.46, blue: 0.53, alpha: 1)
        self.addSubview(messageDate)
        /// messageCount
        count = MessageCountView.init(frame: CGRect.init(x: screenWidth-16-18, y: messageDate.frame.maxY+10, width: 18, height: 18))
        count!.setMessage(num: 9)
        self.addSubview(count!)
        //self.addSubview(messageCount)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /** 设置消息预览视图所包含的信息*/
    func setMessageItem(info : MessageTableCellModel, msgcount : Int) {
        //更新来者的头像
        profileView.image = UIImage.init(named: info.profile)
        //更新来者的昵称
        mateLabel.text = info.title
        //更新消息的内容
        messageLabel.text = "\(info.fromName): \(info.content)"
        //更新时间戳
        let date : String = "\(info.dateTime.split(separator: " ")[0])"
        messageDate.text = date
        //更新未读消息的数量
        count!.setMessage(num: msgcount)
    }
}
