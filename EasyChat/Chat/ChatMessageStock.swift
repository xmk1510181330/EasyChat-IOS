//
//  ChatMessageStock.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/26.
//

import UIKit

/** 聊天消息存储的位置*/
class ChatMessageStock: NSObject {
    /// 公用单例
    static let instance : ChatMessageStock = ChatMessageStock.init()
    ///来自不同好友的消息
    var mateMessageList : [Int : [MessageTableCellModel]] = [:]
    ///群聊头像的数组
    let groupProfileNames : [String] = ["chat_groupProfile0", "chat_groupProfile1"]
    //日期格式转化器
    let dateformatter = DateFormatter()
    
    override init() {
        super.init()
        //使用指定的日期格式化形式
        dateformatter.dateFormat = "YYYY-MM-dd"
    }
    
    func convertC2CResponse(info : NSDictionary) -> EasyChatC2CResponse {
        let result = EasyChatC2CResponse.init()
        result.fromNumber = info.object(forKey: "fromNumber") as! Int
        result.toNumber = info.object(forKey: "toNumber") as! Int
        result.content = info.object(forKey: "content") as! String
        result.dateTime = info.object(forKey: "dateTime") as! String
        return result
    }
    
    func addC2CRequest(c2cRequest : EasyChatC2CRequest) {
        let model = MessageTableCellModel.init()
        let mate = AppDelegate.userInfos[c2cRequest.toNumber]
        if c2cRequest.image != nil { //图片消息
            model.image = c2cRequest.image
        }else { //文本消息
            model.content = c2cRequest.content
        }
        model.dateTime = dateformatter.string(from: Date())
        model.title = mate!.mateName
        model.profile = mate!.matePhote
        model.fromID = c2cRequest.fromNumber
        model.toID = c2cRequest.toNumber
        model.belongID = c2cRequest.toNumber
        model.messageType = 1
        if mate!.mateNumber == AppDelegate.meInfo.number {
            model.fromName = "我"
        }else {
            model.fromName = mate!.mateName
        }
        if mateMessageList[model.belongID] == nil {
            mateMessageList[model.belongID] = [MessageTableCellModel]()
        }
        mateMessageList[model.belongID]?.append(model)
        print("加完了: ", mateMessageList[model.belongID]!.count)
    }
    
    func addC2CResponse(c2cResponse : EasyChatC2CResponse) {
        let model = MessageTableCellModel.init()
        let mate = AppDelegate.userInfos[c2cResponse.fromNumber]
        model.content = c2cResponse.content
        model.dateTime = c2cResponse.dateTime
        model.title = mate!.mateName
        model.profile = mate!.matePhote
        model.fromID = c2cResponse.fromNumber
        model.toID = c2cResponse.toNumber
        model.belongID = model.fromID
        model.messageType = 1
        if mate!.mateNumber == AppDelegate.meInfo.number {
            model.fromName = "我"
        }else {
            model.fromName = mate!.mateName
        }
        if mateMessageList[model.belongID] == nil {
            mateMessageList[model.belongID] = [MessageTableCellModel]()
        }
        mateMessageList[model.belongID]?.append(model)
    }
    
    func addGroupResponse(groupResponse : EasyChatGroupResponse) {
        let model = MessageTableCellModel.init()
        let group = AppDelegate.groupInfos[groupResponse.groupID]
        model.content = groupResponse.content
        model.dateTime = groupResponse.dateTime
        model.title = group!.groupName
        model.fromID = groupResponse.fromNumber
        model.belongID = groupResponse.groupID
        model.messageType = 2
        if groupResponse.fromNumber == AppDelegate.meInfo.number {
            model.fromName = "我"
        }else {
            model.fromName = AppDelegate.userInfos[model.fromID]?.mateName ?? "群友"
        }
        let profileIndex = groupResponse.groupID%2==0 ? 0 : 1
        model.profile = groupProfileNames[profileIndex]
        if mateMessageList[model.belongID] == nil {
            mateMessageList[model.belongID] = [MessageTableCellModel]()
        }
        mateMessageList[model.belongID]?.append(model)
    }
}
