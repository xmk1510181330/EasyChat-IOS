//
//  MessageCountView.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/13.
//

import UIKit

/** 未读消息的小红点*/
class MessageCountView: UIButton {
    /** 留在原地的小圆*/
    let originalView : UIView = UIView.init()
    /** 留在原地的小圆的宽高*/
    var originalViewWH : CGFloat = 0.0
    /** 大圆的尺寸，记录一下备用*/
    var selfSize : CGSize = CGSize.init()
    /** 图标可以拖动的最远距离*/
    var maxDistance : CGFloat = 0.0
    /** 中间不规则矩形的形状*/
    var shapeLayer : CAShapeLayer = CAShapeLayer.init()
    //MARK: - 爆炸特效用到的图片
    lazy var images: [UIImage] = {
        var arraryM = [UIImage]()
        for i in 1...8 {
            let image = UIImage(named: "\(i)")
            arraryM.append(image!)
        }
        return arraryM
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        //设置背景颜色为红色
        self.backgroundColor = UIColor.red
        //设置圆角
        self.layer.masksToBounds = true
        self.layer.cornerRadius = frame.width/2
        //记录原始的尺寸
        selfSize = frame.size
        //默认图标可以拖动的最大距离
        maxDistance = self.layer.cornerRadius * 3
        //设置留在原地的小圆，小圆可以先不加到视图上，拖动开始在开始加
        originalView.backgroundColor = self.backgroundColor
        originalView.frame = CGRect.init(x: self.center.x, y: self.center.y, width: self.layer.cornerRadius, height: self.layer.cornerRadius)
        originalView.layer.masksToBounds = true
        originalView.layer.cornerRadius = originalView.frame.width/2
        originalViewWH = originalView.frame.height
        //设置中间不规则矩形的填充色
        shapeLayer.fillColor = self.backgroundColor?.cgColor
        //设置点击事件
        self.addTarget(self, action: #selector(tapHandler), for: .touchUpInside)
        //设置拖动事件
        let panRecognizer = UIPanGestureRecognizer.init(target: self, action: #selector(panHandler(panGesture:)))
        self.addGestureRecognizer(panRecognizer)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    /** 点击事件*/
    @objc func tapHandler() {
        print("Tap")
    }
    
    /** 设置未读消息数量*/
    func setMessage(num : Int) {
        if num == 0 {
            self.isHidden = true
        }else {
            self.isHidden = false
            self.setTitle("\(num)", for: .normal)
            self.setTitleColor(UIColor.white, for: .normal)
            self.titleLabel?.font = UIFont.systemFont(ofSize: self.layer.cornerRadius+2)
        }
    }
    
    /** 拖动事件*/
    @objc func panHandler(panGesture : UIPanGestureRecognizer) {
        let panPoint = panGesture.translation(in: self)
        
        switch panGesture.state {
        case .began:
            //把留在原地的小圆加到视图上
            self.superview?.insertSubview(originalView, belowSubview: self)  //使用insert保证视图层级
            //把中间的不规则矩形也加上去
            self.superview?.layer.insertSublayer(shapeLayer, below: self.layer)
        case .changed:
            /** 绘制变化图案*/
            //现在点的位置
            let nowPoint = CGPoint.init(x: self.center.x+panPoint.x, y: self.center.y+panPoint.y)
            //设置累加的位置为0
            panGesture.setTranslation(CGPoint.zero, in: self)
            //将原先的图案移动过来
            self.center = nowPoint
            //获取到现在两个圆之间的距离
            let currentDistance = distanceBetween(point1: self.center, point2: originalView.center)
            /** 拖动过程中，大小圆的大小会随着距离的增加而变小*/
            var scale1 = 0.4
            var scale2 = 0.6
            if currentDistance < maxDistance {
                //还在最远距离以内
                scale1 = scale1 + 0.6*((maxDistance-currentDistance)/maxDistance)
                scale2 = scale2 + 0.4*((maxDistance-currentDistance)/maxDistance)
            }
            //设置小圆的新尺寸
            let originalViewNewWH : CGFloat = scale1 * originalViewWH
            originalView.frame.size = CGSize.init(width: originalViewNewWH, height: originalViewNewWH)
            originalView.layer.cornerRadius = originalView.frame.width/2
            //设置大圆的新尺寸
            self.frame.size = CGSize.init(width: selfSize.width*scale2, height: selfSize.height*scale2)
            self.layer.cornerRadius = self.frame.width > self.frame.height ? self.frame.height/2 : self.frame.width/2
            /** 文字的大小*/
            
            if currentDistance > maxDistance {
                //超出最大的距离，移除元素
                originalView.removeFromSuperview()
                shapeLayer.removeFromSuperlayer()
            }else {
                //还在最大距离以内
                shapeLayer.path = drawBetweenRectPath()
            }
        case .ended:
            //获取到现在两个圆之间的距离
            let currentDistance = distanceBetween(point1: self.center, point2: originalView.center)
            if currentDistance < maxDistance {
                //移除掉元素
                originalView.removeFromSuperview()
                shapeLayer.removeFromSuperlayer()
                //回弹特效
                weak var weakSelf = self
                UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.1, initialSpringVelocity: 0, options: .curveEaseInOut) {
                    weakSelf!.center = weakSelf!.originalView.center
                    weakSelf?.frame.size = weakSelf!.selfSize
                    weakSelf!.layer.cornerRadius = weakSelf!.frame.width > weakSelf!.frame.height ? (weakSelf!.frame.height / 2) : (weakSelf!.frame.width / 2)
                } completion: { finish in}
            }else {
                //已经超过最大的限制，移除掉所有元素，并且释放爆炸特效
                killAll()
            }
        default:
            break
        }
    }
    
    /** 移除掉所有元素*/
    func killAll() {
        originalView.removeFromSuperview()
        shapeLayer.removeFromSuperlayer()
        destoryAnimation(location: self.frame)
        self.removeFromSuperview()
    }
    
    /** 获取两个点之间的距离*/
    func distanceBetween(point1 : CGPoint, point2 : CGPoint) -> CGFloat {
        let dx : CGFloat = (point1.x-point2.x)*(point1.x-point2.x)
        let dy : CGFloat = (point1.y-point2.y)*(point1.y-point2.y)
        return sqrt(dx+dy)
    }
    
    /** 绘制大小圆之间的不规则矩形*/
    func drawBetweenRectPath() -> CGPath {
        let center1 = originalView.center
        let r1 = originalView.layer.cornerRadius
        let center2 = self.center
        let r2 = self.layer.cornerRadius
        let d = distanceBetween(point1: center1, point2: center2)
        let x1 = center1.x
        let x2 = center2.x
        let y1 = center1.y
        let y2 = center2.y
        let cosθ = (y2 - y1) / d
        let sinθ = (x2 - x1) / d
        
        let A = CGPoint.init(x: x1 - r1 * cosθ, y: y1 + r1 * sinθ)
        let B = CGPoint.init(x: x1 + r1 * cosθ, y: y1 - r1 * sinθ)
        let C = CGPoint.init(x: x2 + r2 * cosθ, y: y2 - r2 * sinθ)
        let D = CGPoint.init(x: x2 - r2 * cosθ, y: y2 + r2 * sinθ)
        let O = CGPoint.init(x: A.x + d / 2 * sinθ, y: A.y + d / 2 * cosθ)
        let P = CGPoint.init(x: B.x + d / 2 * sinθ, y: B.y + d / 2 * cosθ)
        
        let path = UIBezierPath()
        path.move(to: A)
        path.addLine(to: B)
        path.addQuadCurve(to: C, controlPoint: P)
        path.addLine(to: D)
        path.addQuadCurve(to: A, controlPoint: O)
        return path.cgPath
    }
    
    /** 爆炸特效*/
    func destoryAnimation(location : CGRect) {
        let animationImageView = UIImageView()
        animationImageView.frame = CGRect.init(x: location.midX-34, y: location.midY-25, width: 68, height: 50)
        animationImageView.center = CGPoint.init(x: location.midX, y: location.midY)
        animationImageView.frame.size = CGSize.init(width: 68, height: 50)
        animationImageView.animationImages = images
        animationImageView.animationDuration = 0.5
        animationImageView.animationRepeatCount = 1
        self.superview?.addSubview(animationImageView)
        animationImageView.startAnimating()
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            animationImageView.removeFromSuperview()
        }
    }
}
