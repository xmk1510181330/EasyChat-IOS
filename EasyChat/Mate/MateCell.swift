//
//  MateCell.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/13.
//

import UIKit

/** 好友列表的Cell*/
class MateCell: UITableViewCell {
    /** 头像*/
    let photoImage : UIImageView = UIImageView.init()
    /** 名称*/
    let nameLabel : UILabel = UILabel.init()
    /** 当前状态*/
    let stateLabel : UILabel = UILabel.init()
    /** 个性签名*/
    let sloganLabel : UILabel = UILabel.init()
    /** Model对象*/
    var mateModel : MateModel = MateModel.init()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        /** 内容区域*/
        photoImage.frame = CGRect.init(x: 22+16, y: 10, width: 40, height: 40)
        photoImage.image = UIImage.init(named: "chat_mateProfile2")
        photoImage.layer.masksToBounds = true
        photoImage.layer.cornerRadius = 20
        self.addSubview(photoImage)
        nameLabel.frame = CGRect.init(x: photoImage.frame.maxX+10, y: photoImage.frame.minY+4, width: 10, height: 16)
        nameLabel.text = "昵称"
        nameLabel.font = UIFont.systemFont(ofSize: 16)
        nameLabel.textAlignment = .left
        nameLabel.sizeToFit()
        self.addSubview(nameLabel)
        stateLabel.frame = CGRect.init(x: photoImage.frame.maxX+10, y: nameLabel.frame.maxY+8, width: 65, height: 11)
        stateLabel.text = "状态"
        stateLabel.textColor = UIColor.lightGray
        stateLabel.font = UIFont.systemFont(ofSize: 11)
        stateLabel.textAlignment = .left
        self.addSubview(stateLabel)
        sloganLabel.frame = CGRect.init(x: stateLabel.frame.maxX, y: nameLabel.frame.maxY+8, width: 150, height: 11)
        sloganLabel.textColor = UIColor.lightGray
        sloganLabel.font = UIFont.systemFont(ofSize: 11)
        sloganLabel.textAlignment = .left
        sloganLabel.text = "标语"
        self.addSubview(sloganLabel)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /** 设置好友的信息*/
    func setMate(info : MateModel) {
        mateModel = info
        photoImage.image = UIImage.init(named: mateModel.matePhote)
        nameLabel.text = mateModel.mateName
        stateLabel.text = "[\(mateModel.mateState.rawValue)]"
        sloganLabel.text = mateModel.slogan
    }
}
