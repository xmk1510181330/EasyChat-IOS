//
//  MateCategoryView.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/13.
//

import UIKit

/** 现在的需求就是：点击这个的Header，要把事件传递到父级的Table上进行处理，使用delegate*/
protocol EasyChatMateCategoryDelegate: NSObjectProtocol {
    /** 分组Header的点击事件*/
    func didSelect(categoryView : MateCategoryView, section : Int)
}

/** 实现好友分组的视图*/
class MateCategoryView: UITableViewHeaderFooterView {
    //需要实现的协议
    weak var delegate : EasyChatMateCategoryDelegate?
    /** 当前的section*/
    var section : Int = 0
    /** 名称*/
    let nameLabel : UILabel = UILabel.init()
    /** 好友数量*/
    let countLabel : UILabel = UILabel.init()
    /** 边上的小箭头*/
    let arrowIcon : UIImageView = UIImageView.init()
    /** Model*/
    var categoryModel : MateCategoryModel = MateCategoryModel.init()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        self.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 44)
        self.contentView.backgroundColor = UIColor.white
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        arrowIcon.frame = CGRect.init(x: 22, y: 18, width: 8, height: 8)
        arrowIcon.image = UIImage.init(named: "category_unopen")
        //self.addSubview(arrowIcon)
        /// 这里需要加入到contentView上
        self.contentView.addSubview(arrowIcon)
        nameLabel.frame = CGRect.init(x: arrowIcon.frame.maxX+8, y: arrowIcon.frame.midY-8, width: 100, height: 16)
        nameLabel.textAlignment = .left
        nameLabel.textColor = UIColor.black
        nameLabel.font = UIFont.systemFont(ofSize: 16)
        //nameLabel.sizeToFit()
        //self.addSubview(nameLabel)
        self.contentView.addSubview(nameLabel)
        countLabel.frame = CGRect.init(x: self.frame.width-22-50, y: arrowIcon.frame.midY-5, width: 50, height: 10)
        countLabel.textAlignment = .right
        countLabel.textColor = UIColor.lightGray
        countLabel.font = UIFont.systemFont(ofSize: 10)
        //self.addSubview(countLabel)
        self.contentView.addSubview(countLabel)
        //添加自身的点击事件
        self.isUserInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(tapHandler))
        self.addGestureRecognizer(tapRecognizer)
    }
    
    func setCategory(info : MateCategoryModel, index : Int) {
        section = index
        categoryModel = info
        nameLabel.text = categoryModel.name
        countLabel.text = "\(categoryModel.onlineNum)/\(categoryModel.totleNum)"
    }
    
    @objc func tapHandler() {
        //改变标志位
        categoryModel.isOpen = !categoryModel.isOpen
        //改变展开处的图标
//        arrowIcon.image?.ciImage?.transformed(by: CGAffineTransform.init(rotationAngle: CGFloat.pi/2))
        arrowIcon.image = categoryModel.isOpen ? UIImage.init(named: "category_open") : UIImage.init(named: "category_unopen")
        if self.delegate != nil {
            self.delegate?.didSelect(categoryView: self, section: section)
        }
    }
}
