//
//  MateAddtionView.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/17.
//

import UIKit

/** 附加视图*/
class MateAddtionView: UIView {
    /** 内容视图*/
    let titleLabel : UILabel = UILabel.init()
    let moreIcon : UIImageView = UIImageView.init()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        /**内容视图*/
        titleLabel.frame = CGRect.init(x: 15, y: 18, width: 50, height: 16)
        titleLabel.text = "新朋友"
        titleLabel.font = UIFont.systemFont(ofSize: 16)
        self.addSubview(titleLabel)
        moreIcon.frame = CGRect.init(x: frame.width-12-12, y: 18+2, width: 12, height: 12)
        moreIcon.image = UIImage.init(named: "category_more")
        self.addSubview(moreIcon)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    convenience init(title : String, frame : CGRect) {
        self.init(frame: frame)
        titleLabel.text = title
    }
}
