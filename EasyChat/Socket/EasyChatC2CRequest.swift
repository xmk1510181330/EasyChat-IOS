//
//  EasyChatC2CRequest.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/18.
//

import UIKit
import YYModel

class EasyChatC2CRequest: NSObject, YYModel {
    @objc var fromNumber : Int = 4141
    @objc var toNumber : Int = 9949
    @objc var content : String = "欢迎各位同学报考郑州轻工业大学🏸️"
    @objc var image : UIImage?
    
    override init() {
        super.init()
    }
}
