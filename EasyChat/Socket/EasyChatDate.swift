//
//  EasyChatDate.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/23.
//

import UIKit

/** 日期类*/
class EasyChatDate: NSObject {
    var year : Int = 1970
    var month : Int = 1
    var day : Int = 1
    var hour : Int = 0
    var minute : Int = 0
    var second : Int = 0
    var nano : Int = 0
}
