//
//  ChatResponse.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/25.
//

import UIKit

class ChatResponse: NSObject {
    @objc var nums : Int = 1
    @objc var responses : [EasyChatC2CResponse] = []
}
