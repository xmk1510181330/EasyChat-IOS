//
//  EasyChatC2CResponse.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/23.
//

import UIKit

/** 私聊消息的响应*/
class EasyChatC2CResponse: NSObject {
    /** 消息来源*/
    @objc var fromNumber : Int = 10001
    /** 消息去向*/
    @objc var toNumber : Int = 10002
    /** 消息内容*/
    @objc var content : String = ""
    /** 消息时间戳*/
    @objc var dateTime : String = ""
}
