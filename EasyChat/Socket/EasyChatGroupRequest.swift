//
//  EasyChatGroupRequest.swift
//  EasyChat
//
//  Created by vase on 2022/1/4.
//

import UIKit
import YYModel

class EasyChatGroupRequest: NSObject, YYModel {
    @objc var fromNumber : Int = 4141
    @objc var groupID : Int = 3
    @objc var content : String = "欢迎各位同学报考郑州轻工业大学🏸️"
    
    override init() {
        super.init()
    }
}
