//
//  EasyChatGroupResponse.swift
//  EasyChat
//
//  Created by vase on 2022/1/4.
//

import UIKit

class EasyChatGroupResponse: NSObject {
    /** 消息的发送者*/
    @objc var fromNumber : Int = 2
    /** 消息所属的群聊*/
    @objc var groupID : Int = 3
    /** 消息的内容*/
    @objc var content : String = ""
    /** 消息图片内容*/
    @objc var image : UIImage?
    /** 消息时间戳*/
    @objc var dateTime : String = ""
}
