//
//  MateCategoryModel.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/13.
//

import UIKit

class MateCategoryModel: NSObject {
    /** 分组名称*/
    var name : String = "我的好友"
    /** 分组是否展开*/
    var isOpen : Bool = false
    /** 分组总人数*/
    var totleNum : Int = 0
    /** 在线人数*/
    var onlineNum : Int = 0
}
