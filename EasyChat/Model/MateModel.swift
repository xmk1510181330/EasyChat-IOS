//
//  MateModel.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/13.
//

import UIKit

/** 用户状态的枚举*/
enum UserOnlineState : String {
    case learn = "📖 学习中"
    case holiday = "⛱️ 度假中"
    case basketball = "🏀 打球中"
    case game = "🎮 游戏中"
    case video = "📺 追剧中"
    case mahjong = "🀄️ 麻将"
    case sleep = "😪 睡觉中"
    case love = "💓 恋爱中"
    case travel = "⛰️ 旅游中"
    case emo = "😫 emo"
}

class MateModel: NSObject {
    /** 好友号码*/
    var mateNumber : Int = 9949
    /** 好友昵称*/
    var mateName : String = "摇红"
    /** 好友头像*/
    var matePhote : String = "chat_mateProfile2"
    /** 好友状态*/
    var mateState : UserOnlineState = .sleep
    /** 是否在线*/
    var online : Bool = true
    /** 个性签名*/
    var slogan : String = "雄关漫道真如铁，而今迈步从头越"
    
    /** 无参构造器*/
    override init() {
        super.init()
    }
    
    /** 含有部分参数的构造器*/
    convenience init(number : Int, name : String, photo : String, state : UserOnlineState) {
        self.init()
        mateNumber = number
        mateName = name
        matePhote = photo
        mateState = state
    }
}
