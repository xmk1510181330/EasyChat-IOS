//
//  MessageTableCellModel.swift
//  EasyChat
//
//  Created by vase on 2022/1/4.
//

import UIKit

/**
 * 消息可以分成两种，自己发送出去的消息和接收到的来自别人的消息
 */
/** 列表显示Cell的Model*/
class MessageTableCellModel: NSObject {
    /** 头像图片*/
    var profile : String = ""
    /** 消息来源，私聊消息时表示消息发送者，群聊消息时表示群名称*/
    var title : String = ""
    /** 消息的内容*/
    var content : String = ""
    /** 图片消息*/
    var image : UIImage?
    /** 消息来源*/
    var fromName : String = ""
    var fromID : Int = -1
    /** 消息的去向*/
    var toID : Int = -1
    /** 消息归属*/
    var belongID : Int = -1
    /** 消息类型，1为私聊消息，2为群聊消息*/
    var messageType : Int = 1
    /** 消息时间戳*/
    var dateTime : String = ""
}
