//
//  AuthModel.swift
//  EasyChat
//
//  Created by vase on 2022/1/21.
//

import UIKit

class AuthModel: NSObject {
    /** 邮箱*/
    var emailAddress : String = ""
    /** 密码*/
    var password : String = ""
    /** 手机号码*/
    var phoneNumber : String = "13663751068"
    /** 区号*/
    var regionPhoneCode : String = "86"
    /** 验证码*/
    var smsCode : String = "159263"
}
