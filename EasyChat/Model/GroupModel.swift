//
//  GroupModel.swift
//  EasyChat
//
//  Created by vase on 2022/1/4.
//

import UIKit

/** 群组消息的Model*/
class GroupModel: NSObject {
    /** 群组唯一标识号码*/
    var groupNumber : Int = 3
    /** 群组名称*/
    var groupName : String = "华山派内部交流群"
    /** 群主*/
    var groupOwner : Int = 2
    /** 群组创建时间*/
    var groupCreateTime : String = "2022-1-4"
    
    /** 无参构造器*/
    override init() {
        super.init()
    }
    
    /** 含有部分参数的构造器*/
    convenience init(number : Int, name : String, ownerID : Int, createTime : String) {
        self.init()
        groupNumber = number
        groupName = name
        groupOwner = ownerID
        groupCreateTime = createTime
    }
}
