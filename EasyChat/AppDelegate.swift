//
//  AppDelegate.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/7.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static var userInfos : [Int : MateModel] = [:]
    static var groupInfos : [Int : GroupModel] = [:]
    static var meInfo : EasyChatReportInfo = EasyChatReportInfo.init()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        //设置机型适配
        ViewConstant.fitScreen(nowWindow: window!)
        //连接网络
        EasyChatSocketTool.instance.connectHost()
        //获取OSS的token
//        NetWorkTools.shared.get(path: ServerRoute.auth.oss, params: [:]) { success, data in
//            if success, let ossToken = data["ossToken"] as? String {
//                UserDefaults.standard.set(ossToken, forKey: "ossToken")
//            }
//        }
        //初始化数据
        initData()
        //检查权限
        AppCheck.instance.notifyCheck()
        let tabberController = UITabBarController();
        tabberController.tabBar.backgroundColor = UIColor.init(red: 0.96, green: 0.97, blue: 0.99, alpha: 1)
        //setviews
        let chatVC = ChatViewController()
        let mateVC = MateViewController()
        let zoneVC = ZoneViewController()
        tabberController.setViewControllers([chatVC, mateVC, zoneVC], animated: true);
        
        let navigationController = UseDefineNavigation(rootViewController: tabberController);
        navigationController.isNavigationBarHidden = true
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        return true
    }
    
    func initData() {
        //一些好友对应的信息设置
        let liuyue = MateModel.init(number: 4141, name: "刘玥", photo: "chat_mateProfile1", state: .emo)
        AppDelegate.userInfos[liuyue.mateNumber] = liuyue
        let lhc = MateModel.init(number: 4346, name: "令狐冲", photo: "chat_mateProfile3", state: .game)
        AppDelegate.userInfos[lhc.mateNumber] = lhc
        let ybq = MateModel.init(number: 6571, name: "岳不群", photo: "chat_mateProfile4", state: .holiday)
        AppDelegate.userInfos[ybq.mateNumber] = ybq
        let lqs = MateModel.init(number: 9949, name: "刘青松", photo: "chat_mateProfile2", state: .sleep)
        AppDelegate.userInfos[lqs.mateNumber] = lqs
        let report = EasyChatReportInfo.init()
        AppDelegate.userInfos[report.number] = AppDelegate.userInfos[report.number]
        let group1 = GroupModel.init()
        AppDelegate.groupInfos[3] = group1
    }
}

