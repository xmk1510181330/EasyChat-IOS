//
//  NetWorkTools.swift
//  EasyChat
//
//  Created by vase on 2022/1/20.
//

import UIKit
import Alamofire

/** 请求类型的枚举*/
struct RequestType {
    static let GET : String = "GET";
    static let POST : String = "POST";
}

/** 请求相应的回调*/
typealias NetworkCompleteBlock = (_ success: Bool, _ data: [String:AnyObject]) -> Void

class NetWorkTools: NSObject {
    /** 全局单例*/
    static let shared = NetWorkTools();
    /** 请求前缀*/
    //let baseUrl = "http://127.0.0.1:2884/";
    
    /** GET请求*/
    func get(path : String, params : [String : AnyObject], complete : @escaping NetworkCompleteBlock) {
        let encoding : ParameterEncoding = URLEncoding(destination: .queryString, arrayEncoding: .noBrackets, boolEncoding: .numeric)
        let header : HTTPHeaders = [:]
        Alamofire.request(path, method: .get, parameters: params, encoding: encoding, headers: header).responseJSON { response in
            guard let value = response.value as? [String : AnyObject] else{
                //请求失败
                return ;
            }
            if let status = value["code"] as? Int {
                if status == 200 {
                    complete(true, value)
                }else {
                    complete(false, value)
                }
            }
        }
    }
    
    /** POST请求*/
    func post(path : String, params : [String : AnyObject], complete : @escaping NetworkCompleteBlock) {
        let encoding = JSONEncoding.default
        let header : HTTPHeaders = [:]
        Alamofire.request(path, method: .post, parameters: params, encoding: encoding, headers: header).responseJSON { response in
            guard let value = response.value as? [String : AnyObject] else{
                //请求失败
                return ;
            }
            print(value)
        }
    }

    override init() {
        super.init()
    }
}

/** 网络请求路由表*/
struct ServerRoute {
    /** Auth模块的网络路由*/
    //static var authUrl = "http://127.0.0.1:2884"
    static var authUrl = "http://lhprint-dev.lymanprinter.com/api"
    struct auth {
        static var login : String = "\(ServerRoute.authUrl)/user/login"
        static var register : String = "\(ServerRoute.authUrl)/user/register"
        static var oss : String = "\(ServerRoute.authUrl)/user/oss"
        static let authBySmsCode : String = "\(ServerRoute.authUrl)/account/v1/auth/login/code"
    }
}
