//
//  ZoneViewController.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/7.
//

import UIKit

class ZoneViewController: UIViewController {
    /** 顶部视图*/
    let topView : UIView = UIView.init()
    let titleLabel : UILabel = UILabel.init() //顶部Title
    /** 内容视图*/
    let mainView : UIView = UIView.init()

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.tabBarItem = UITabBarItem.init(title: "动态", image: UIImage.init(named: "zone_unsel")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage.init(named: "zone_sel")?.withRenderingMode(.alwaysOriginal))
        self.view.backgroundColor = UIColor.init(red: 0.07, green: 0.64, blue: 1, alpha: 1)
        
        /** 顶部视图*/
        topView.frame = CGRect.init(x: 0, y: stateRegionHeight, width: screenWidth, height: 92-stateRegionHeight)
        topView.backgroundColor = UIColor.init(red: 0.07, green: 0.64, blue: 1, alpha: 1)
        self.view.addSubview(topView)
        /// 顶部Title
        titleLabel.frame = CGRect.init(x: (screenWidth-60)/2, y: 12, width: 60, height: 18)
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.systemFont(ofSize: 18)
        titleLabel.text = "动态"
        topView.addSubview(titleLabel)
        
        mainView.frame = CGRect.init(x: 0, y: topView.frame.maxY, width: screenWidth, height: safeScreenHeight)
        mainView.backgroundColor = UIColor.white
        self.view.addSubview(mainView)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
}
