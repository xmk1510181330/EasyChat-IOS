//
//  ViewConstant.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/7.
//

import UIKit

/** 状态栏高度*/
public var stateRegionHeight : CGFloat = 20
/** 底部导航栏高度*/
public var bottomRegionHeight : CGFloat = 0
/** 屏幕宽度*/
public var screenWidth : CGFloat = 100
/** 屏幕高度*/
public var screenHeight : CGFloat = 400

public var safeScreenHeight : CGFloat = 100
/** 安全域信息*/
var bottom : CGFloat = 0
var top : CGFloat = 0

class ViewConstant: NSObject {

    /** 屏幕适配*/
    static func fitScreen(nowWindow : UIWindow) {
        if isPhone(window: nowWindow) {
            stateRegionHeight = top
            bottomRegionHeight = bottom
        }
        //设置屏幕的宽高
        screenWidth = nowWindow.bounds.width
        screenHeight = nowWindow.bounds.height
        safeScreenHeight = screenHeight - stateRegionHeight - bottomRegionHeight - 49
        //print(stateRegionHeight)
    }
    /**
     * 1. 使用屏幕的高度来判断机型，枚举IPhone及以后所有机型的高度
     * 2. 使用手机的型号来实现判别，枚举IPhone及以后的所有型号
     * 3. 通过是否存在安全域的概念判断是否是IPhone及以后的机型，不需要枚举
     */
    static func isPhone(window : UIWindow) -> Bool {
        if #available(iOS 11.0, *) {
            //如果当前机型存在安全域，那么就是刘海屏
            if window.safeAreaInsets.bottom > 0.0 {
                top = window.safeAreaInsets.top
                bottom = window.safeAreaInsets.bottom
                return true
            }
        } else {
            // Fallback on earlier versions
        }
        return false
    }
}
