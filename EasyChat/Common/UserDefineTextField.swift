//
//  UserDefineTextField.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/7.
//

import UIKit

/** 自定义的文本输入框*/
class UserDefineTextField: UITextField {
    
    var fieldWidth : CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        fieldWidth = frame.width
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    //修改这个占位符的位置
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.placeholderRect(forBounds: bounds)
        rect = CGRect.init(x: 8, y: rect.origin.y, width: rect.width, height: rect.height)
        return rect
    }
    
    //修改移动光标的位置
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        //let result = CGRect.init(x: bounds.origin.x+6, y: bounds.origin.y, width: bounds.width-15, height: bounds.height)
        let result = CGRect.init(x: bounds.origin.x+9, y: bounds.origin.y, width: bounds.width-15, height: bounds.height)
        return result
    }
    
    //修改输入文本的位置，一般和修改光标位置的方法一起用
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let result = CGRect.init(x: bounds.origin.x+9, y: bounds.origin.y, width: bounds.width, height: bounds.height)
        return result
    }
}
