//
//  UseDefineNavigation.swift
//  EasyChat
//
//  Created by it-go-0367 on 2021/12/7.
//

import UIKit

class UseDefineNavigation: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    //重写这个方法就可以修改原来的
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        //viewController.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "返回", style: .done, target: nil, action: nil)
        super.pushViewController(viewController, animated: true)
    }
}
